# README #

GuitarShoppe is an e-commerce website for guitars. It is a Java Web Application which runs on Apache Tomcat 8.0 and MySQL 6.3 database connection. 

You can have a preview of this website on [OpenShift](http://guitarshoppe-tvadakke.rhcloud.com/GuitarShoppe/index.jsp).

### Website Overview ###

* User can check the products according to its categories
* User can update items in his cart
* User needs to login/sign up to place an order
* User can review his purchase/order
* User receives an email once his order is placed successfully
* The website is protected from Cross-site scripting attacks, SQL injection attacks and Social Engineering attacks


### Developed on? ###

* Java EE JDK version 1.8
* Apache Tomcat 8.0 
* MySQL 6.3.5


### Who do I talk to? ###

* [Thomson Varghese](https://bitbucket.org/tomzapple/) (thomsonvarghese2005@gmail.com)
* [Anuradha Pai](https://bitbucket.org/kapai/) (pai.anuradha248@gmail.com)