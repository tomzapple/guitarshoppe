<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<jsp:include page="/include/header.jsp" />
<jsp:include page="/include/user-navigation.jsp" />
<jsp:include page="/include/site-navigation.jsp" />


<section>

    <div>
        <h3> Choose your Guitar Type </h3>
        <form name="action" action="<c:url value='/CatalogController'/>" method="GET">
            <select id="guitar_type_dropdown" name="catalogCategory" size="1" onchange="document.action.submit();">
                <c:forEach var="item" items="${catalogCategories}">
                    <option value="${item.key}" ${item.key == selectedCategory ? 'selected="selected"' : ''}>${item.value}</option>
                </c:forEach>
                <!--                <option value="all">All</option>
                                <option value="acoustic">Acoustic</option>
                                <option value="electric">Electric</option>
                                <option value="classical">Classical or Spanish</option>-->
                <!--                <input type="submit" value="click">-->
            </select>
        </form>
    </div>
    <br>
    <table class="cartitems" >
        <thead>
            <tr>
                <th>Product</th>
                <th>Category</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="item" items="${productListOfCategory}">
                <tr>
                    <td><img src="<c:url value='${item.imageURL}'/>" alt="${item.name}" </td>
                    <td>
                        <a href="<c:url value='/CatalogController?productCode=${item.productCode}'/>">
                            ${item.name}</a></td>
                    <td>                    ${item.price}</td>

                </tr>
            </c:forEach>

        </tbody>
    </table>

</section>
<jsp:include page="/include/footer.jsp" />