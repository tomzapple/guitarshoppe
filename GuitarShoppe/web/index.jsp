<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<jsp:include page="/include/header.jsp" />
<jsp:include page="/include/user-navigation.jsp" />
<jsp:include page="/include/site-navigation.jsp" />
<section>
    <h2>Welcome to Guitar Shoppè !</h2>

    <p>Offering the best of the best in music instruments and gear. One of the most popular instrument ever known to the world, the guitar is something truly special. Versatile in its ability to play different genres, and coming in a variety of styles, every guitar has something different to offer, allowing musicians to make their individual instrument truly their own. From highly sought after models, to everyday pick up and play options, the guitars found here were made to accommodate literally every need a guitarist could ever have. 
        So where do you start in a section as massive as this one? From acoustic to electric, nylon to steel stringed, hollow body to solid body, all styles are represented here, so having a good idea of what you're looking for will definitely help. The best place to start is usually with the brand, as each one has a reputation for something different, allowing you to narrow things down from there. For example, you'll instantly recognize names like Fender, Gibson and Ibanez as trailblazers of the electric guitar, while others like Breedlove, Luna Guitars and Rainsong are more famous for the unparalleled quality of their acoustic instruments. After that, you'll want to look at things like body type, tonewood, strings, size, orientation and performance level. 
        Once you have a general idea about each of these, your decision gets that much easier. Just remember, it all comes down to personal preference, so as long as you're happy with the guitar you choose you can't go wrong. The guitar is a special instrument, with a different meaning to every player. Whether you're taking your first steps into the world of music or you're a professional rocking out in front of sold out stadiums all over the planet, strumming, plucking and picking on a guitar is a way to express yourself that can't be duplicated with anything else. So pick up a guitar here and start playing... you'll be glad you did.
    </p>
</section>

<jsp:include page="/include/footer.jsp" />