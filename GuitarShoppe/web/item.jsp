<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<jsp:include page="/include/header.jsp" />
<jsp:include page="/include/user-navigation.jsp" />
<jsp:include page="/include/site-navigation.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <form name="item" action="<c:url value='/CatalogController'/>" method="POST">
        <input type="submit" value="Back">
    </form>
    <figure>

        <img src="<c:url value='${product.imageURL}'/>" alt=${product.name} width="304" height="400">
        <!--                <figcaption>The Ibanez GD10 Dreadnought Acoustic Guitar</figcaption>-->
    </figure>
    <h2> ${product.name} </h2>
    <h3> ${product.category} Guitar </h3> <br> <br>
    <h4> ${product.price}  </h4>



    <form action="<c:url value='/OrderController'/>" method="POST">
        <input type="hidden" name="action" value="updateCart">
        <input type="hidden" name="productList" value="<c:out value='${product.productCode}'/>">
        <input class="cartitems_buttons" type="submit" value="Add To Cart"  />
    </form>

    <blockquote cite=http://www.guitarcenter.com/Ibanez-GD10-Dreadnought-Acoustic-Guitar-107886707-i2478581.gc>
        <!--        The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. 
                Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.-->
        ${product.description}
    </blockquote>
</section>
<jsp:include page="/include/footer.jsp" />
