<%-- 
    Document   : order
    Created on : Feb 19, 2015, 10:36:11 PM
    Author     : Anuradha
--%>

<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<jsp:include page="/include/header.jsp" />
<jsp:include page="/include/user-navigation.jsp" />
<jsp:include page="/include/site-navigation.jsp" />
<section>
    <h3 > Invoice </h3><br>

    <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <fmt:formatDate value="${currentOrder.date}" var="formattedDate" 
                    type="date" pattern="MM-dd-yyyy" />

    <p> Date : ${formattedDate}</p><br>
    <h4> Ship To/ Bill To:</h4> <br>
    <h4>${currentOrder.user.firstName} ${currentOrder.user.lastName}</h4> <br>
    <h4>${currentOrder.user.address1}</h4> <br>
    <h4>${currentOrder.user.address2}</h4> <br>
    <h4>${currentOrder.user.city}</h4> <br>
    <h4>${currentOrder.user.state}</h4> <br>
    <h4>${currentOrder.user.postCode}</h4> <br>
    <h4>${currentOrder.user.country}</h4> <br>




    <table class = "cartitems" >
        <thead>
            <tr>
                <th>Guitar Name</th>
                <th>Cost</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
            <c:forEach var="item" items="${sessionScope.theShoppingCart.orderItems}">
            <form action="<c:url value='/OrderController'/>" method="POST">
                <tr>
                    <td> ${item.product.name}</td>
                    <td> ${item.product.price}</td>

                    <td><input type="text" name="<c:out value='${item.product.productCode}'/>" value="<c:out value='${item.quantity}'/>" /></td>
                <input type="hidden" name="productList" value="<c:out value='${item.product.productCode}'/>">
                <td>${item.total}</td>

                </tr>
            </c:forEach>
            <!--            <tr id="cart_items_last_row">
                            <td>Ibanez GD10 Dreadnought Acoustic Guitar</td>
                            <td>$100</td>
                            <td><input type="text" name="Quantity" value="2" /></td>
                            <td>$200</td>
                        </tr>-->
            <tr>
                <td></td>
                <td></td>
                <td>Subtotal</td>
                <td>${currentOrder.totalCost}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Tax</td>
                <td>${currentOrder.totalCost * currentOrder.taxRate}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>Total</td>
                <td>${currentOrder.totalCost + (currentOrder.totalCost*currentOrder.taxRate)}</td>
            </tr>

            <tr>
                <td></td>
                <td></td>
                <td></td>

                <td>                             <form action="<c:url value='/OrderController'/>">
                        <input class="cartitems_buttons" type="submit" value="Back To Cart" />
                    </form></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>                             <form action="">
                        <input class="cartitems_buttons" type="submit" value="Purchase" />
                    </form></td>
            </tr>
            </tbody>
    </table>


</section>

<jsp:include page="/include/footer.jsp" />