<%-- 
    Document   : header
    Created on : Feb 19, 2015, 9:10:09 PM
    Author     : Anuradha
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="Styles/main.css ">
        <title>Home</title>
    </head>
    <body>
        <header>
            <div id="sign_in_status">Not Signed In</div>
            <h1>The Guitar Shopp�</h1>
            <p>Strings for the soul...</p>
            <nav id="breadcrumbs">
                <p>
                    <a class="selected" href="">Home</a>
                </p>
            </nav>

        </header>
