<%-- 
    Document   : site-navigation
    Created on : Feb 19, 2015, 9:25:52 PM
    Author     : Anuradha
--%>
<aside id="sidebar">
    <nav>
        <ul>
            <li><a class="selected" href="index.jsp">
                    Home</a></li>
            <li><a href="CatalogController">
                    Store</a></li>
            <li><a href="about.jsp">
                    About</a></li>
            <li><a href="contact.jsp">
                    Contact Us</a></li>
        </ul>
    </nav>
</aside>
