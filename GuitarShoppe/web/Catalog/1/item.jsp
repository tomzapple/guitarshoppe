<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%> 
<c:import url="/include/header.jsp" />
<c:import url="/include/user-navigation.jsp" />
<c:import url="/include/site-navigation.jsp" />
<section>
    <form name="item" action="catalog.jsp">
        <input type="submit" value="Back">
    </form>
    <figure>
        <img src="images/acoustic_01.jpg" alt="Ibanez GD10 Dreadnought Acoustic Guitar" width="304" height="400">
        <!--                <figcaption>The Ibanez GD10 Dreadnought Acoustic Guitar</figcaption>-->
    </figure>
    <h2> Ibanez GD10 Dreadnought Acoustic Guitar </h2>
    <h3> Acoustic Guitar </h3> <br> <br>
    <h4> $100 </h4>
    <form action="cart.jsp">
        <input class="cartitems_buttons" type="submit" value="Add To Cart"  />
    </form>

    <blockquote cite=http://www.guitarcenter.com/Ibanez-GD10-Dreadnought-Acoustic-Guitar-107886707-i2478581.gc>
        The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. 
        Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.
    </blockquote>

</section>
<c:import url="/include/footer.jsp" />
