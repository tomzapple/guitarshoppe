<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<%@page contentType="text/html" pageEncoding="utf-8"%>
<!DOCTYPE html>
<jsp:include page="/include/header.jsp" />
<jsp:include page="/include/user-navigation.jsp" />
<jsp:include page="/include/site-navigation.jsp" />
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<section>
    <h3 > You have added the following to your cart </h3>
    <table id = "cartitems" >
        <thead>
            <tr>
                <th>Guitar Name</th>
                <th>Cost</th>
                <th>Quantity</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>

            <c:forEach var="item" items="${sessionScope.theShoppingCart.orderItems}">

            <form action="<c:url value='/OrderController'/>"  method="POST">
                <tr>
                    <td> ${item.product.name}</td>
                    <td> ${item.product.price}</td>


                    <td><input type="text" name="<c:out value='${item.product.productCode}'/>" value="<c:out value='${item.quantity}'/>"/></td>

                <input type="hidden" name="productList" value="<c:out value='${item.product.productCode}'/>">
                <td>${item.total}</td>

                </tr>
            </c:forEach>

            <td>    
                <input type="hidden" name="action" value="updateCart">
                <input type="hidden" name="productList" value="<c:out value='${productList}'/>">
                <input class="cartitems_buttons" type="submit" value="Modify Cart" />
            </td>
            </tr>
        </form>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td>    <form action="<c:url value='/OrderController'/>" method="POST">
                    <input type="hidden" name="action" value="checkout">
                    <input class="cartitems_buttons" type="submit" value="Checkout" />
                </form></td>

        </tr>
        </tbody>
    </table> 

</section>
<jsp:include page="/include/footer.jsp" />