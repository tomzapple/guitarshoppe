/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.business;

import java.io.Serializable;

/**
 *
 * @author thomsonverghese
 */
public class Product implements Serializable {

    private long productCode;

    private String name;

    private String category;

    private String description;

    private double price;

    private String imageURL;

    public Product(long productCode, String name, String category, String description, double price, String imageURL) {
        this.productCode = productCode;
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.imageURL = imageURL;
    }

    public Product() {
        this.productCode = Long.MIN_VALUE;
        this.name = "";
        this.category = "";
        this.description = "";
        this.price = 0.00;
        this.imageURL = "";
    }
    
    

    public String getImageURL() {
        String imageURL = "/images/" + productCode + ".jpg";
        return imageURL;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getProductCode() {
        return productCode;
    }

    public void setProductCode(long productCode) {
        this.productCode = productCode;
    }

}
