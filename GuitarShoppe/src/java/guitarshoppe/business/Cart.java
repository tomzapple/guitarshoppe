/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author thomsonverghese
 */
public class Cart implements Serializable {

    List<OrderItem> orderItems;

    public Cart(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Cart() {
        this.orderItems = new LinkedList<OrderItem>();
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    //        The cart should not allow multiple OrderItems for the same product, but should update
//appropriately if one already exists.
    public void addItem(Product product, int quantity) {
        boolean foundProduct = false;
        for (OrderItem orderItem : this.orderItems) {
            if (orderItem.getProduct().getProductCode()== (product.getProductCode())) {
//                long oldQuantity = orderItem.getQuantity();
//                if(oldQuantity == quantity)
                orderItem.setQuantity(quantity);
                foundProduct = true;
            }
        }
        //If the product doesn’t already exist in the cart, the current cart quantity should be considered zero to begin.
        if (!foundProduct) {
            this.orderItems.add(new OrderItem(product, quantity));
        }

    }

    //removes any OrderItem associated with the given Product.   
    public void removeItem(Product product) {
        OrderItem itemToBeRemoved = null;
        for (OrderItem item : this.orderItems) {
            if (item.getProduct().getProductCode() == product.getProductCode()) {
                itemToBeRemoved = item;
            }
        }
        if (itemToBeRemoved != null) {
            this.orderItems.remove(itemToBeRemoved);
        }

    }

    //clears the entire cart contents
    public void emptyCart() {

    }

    public long getQuantity(Product product) {
        for (OrderItem orderItem : this.orderItems) {
            if (orderItem.getProduct().getProductCode() == product.getProductCode()) {
                return orderItem.getQuantity();
            }
        }
        //the product never exists
        return -1;
    }

    public ArrayList<String> getProductCodeList() {
        ArrayList<String> productCodeList = new ArrayList<String>();
        for (OrderItem orderItem : this.orderItems) {
            String prodCode = Long.toString(orderItem.getProduct().getProductCode());
            productCodeList.add(prodCode);
        }
        return productCodeList;
    }

}
