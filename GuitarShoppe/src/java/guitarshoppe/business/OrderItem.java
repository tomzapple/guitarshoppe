/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.business;

import java.io.Serializable;

/**
 *
 * @author thomsonverghese
 */
public class OrderItem implements Serializable {

    private Product product;

    private long quantity;

    private double total;

    public OrderItem(Product product, long quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public OrderItem() {
        this.product = new Product();
        this.quantity = 0;
        this.total = 0.0;
    }

    public double getTotal() {
        return this.product.getPrice() * this.quantity;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
