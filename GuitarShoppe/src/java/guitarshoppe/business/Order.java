/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.business;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author thomsonverghese
 */
public class Order implements Serializable {

    private String orderNumber;

    private Date date;

    private User user;

    private List<OrderItem> orderItems;

    private double taxRate;

    private double totalCost;

    private boolean paid;

    public Order(String orderNumber, Date date, User user, List<OrderItem> orderItems, double taxRate, boolean paid) {
        this.orderNumber = orderNumber;
        this.date = date;
        this.user = user;
        this.orderItems = orderItems;
        this.taxRate = taxRate;
        this.paid = paid;
    }

    public Order() {

        this.orderNumber = "";
        this.date = new Date();
        this.user = new User();
        this.orderItems = null;
        this.taxRate = 0.0;
        this.totalCost = 0.0;
        this.paid = false;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public double getTotalCost() {

        double total = 0.0;
        for (OrderItem orderItem : orderItems) {
            total += orderItem.getTotal();
        }
         this.totalCost = total;
         return this.totalCost;
    }

    public double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(double taxRate) {
        this.taxRate = taxRate;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

}
