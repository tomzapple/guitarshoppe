/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.controllers;

import guitarshoppe.business.Cart;
import guitarshoppe.business.Order;
import guitarshoppe.business.Product;
import guitarshoppe.business.User;
import guitarshoppe.data.ProductDB;
import guitarshoppe.data.UserDB;
import java.io.IOException;
import java.time.Instant;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author thomsonverghese
 */
public class OrderController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.html";

        HttpSession session = request.getSession();

        //Checks the session for a current shopping cart, using the attribute “theShoppingCart”
        Cart shoppingCart = (Cart) session.getAttribute("theShoppingCart");

        if (shoppingCart == null) {
            shoppingCart = new Cart();
            session.setAttribute("theShoppingCart", shoppingCart);
        } else {

        }

        //Checks the http request for a parameter called “action”
        String action = request.getParameter("action");

        if (action != null) {
            if (action.equals("updateCart")) {
                //If there is an action parameter, validate that its value is either “updateCart” or
//“checkout”
                String[] productCodeList = request.getParameterValues("productList");
                for (String productCode : productCodeList) {
                    String strippedProductCode = productCode.replaceAll("\\D+", "");
                    long productCodeValue = -1;
                    try {
                        productCodeValue = Long.parseLong(strippedProductCode);

                    } catch (NumberFormatException numberFormatException) {

                    }
                    Product product = ProductDB.getProduct(productCodeValue);
                    if (product != null) {
                        // we have a valid product

                        //get the product quantity
                        //The quantity associated with each product code for potential update should be stored in the request as aparameter named exactly as the product code itself
                        String quantityString = (String) request.getParameter(strippedProductCode);

                        int quantityValue;
                        try {
                            quantityValue = Integer.parseInt(quantityString);
                            if (quantityValue < 0) {
                                quantityValue = 1;
                            }
                        } catch (NumberFormatException numberFormatException) {
                            //will be handled if the quantity string is null
                            quantityValue = 1;
                        }
                        //If the request parameter quantity is zero, remove the product from the cart (if present) and move on to the next product code.
                        if (quantityValue == 0) {
                            shoppingCart.removeItem(product);

                        } //                        else if (shoppingCart.getQuantity(product) == -1) {
                        //                            //If the product doesn’t already exist in thecart, the current cart quantity should beconsidered zero to begin.
                        //                            shoppingCart.addItem(product, 0);
                        //                        } 
                        else if (quantityString == null) {
                            //If there was no quantity specific request parameter, add 1 to the quantity of the product in the cart.
                            int tempQuantity = 0;
                            if (shoppingCart.getQuantity(product) == -1){
                                tempQuantity = 1;
                            }else{
                                tempQuantity = (int) (shoppingCart.getQuantity(product) + 1);
                            }
                            shoppingCart.addItem(product, tempQuantity);
                        } else if (quantityValue != (int)shoppingCart.getQuantity(product)) {
                            shoppingCart.addItem(product, quantityValue);
                        }

                    }
                }
//                request.setAttribute("cart", url);

                session.setAttribute("productList", shoppingCart.getProductCodeList());
                session.setAttribute("theShoppingCart", shoppingCart);
                url = "/cart.jsp";
            } else if (action.equals("checkout")) {
                User user = UserDB.getUsers().get(0);
                session.setAttribute("theUser", user);
                Order order = new Order("00001", Date.from(Instant.EPOCH), user, shoppingCart.getOrderItems(), .025, true);
                session.setAttribute("currentOrder", order);
                url = "/order.jsp";
            }
        } else {
            //If there is no action parameter, or if it has an unknown value, dispatch directly to
//the shopping cart JSP view.
            url = "/cart.jsp";
        }

//        if (action.equals("add")) {
//            // get parameters from the request
//            String firstName = request.getParameter("firstName");
//            String lastName = request.getParameter("lastName");
//            String email = request.getParameter("email");
//
//            url = "/thanks.jsp";
//        } else if (action.equals("join")) {
//            // set URL to index page
//            url = "/index.html";
//        }
        // forward request and response objects
        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
