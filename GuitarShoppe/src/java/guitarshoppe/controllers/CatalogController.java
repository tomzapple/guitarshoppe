/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.controllers;

import guitarshoppe.business.Product;
import guitarshoppe.data.ProductDB;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author thomsonverghese
 */
public class CatalogController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        String url = "/index.html";

        //Checks the http request for a parameter called “productCode”
        String productCode = request.getParameter("productCode");
        String catalogCategory = request.getParameter("catalogCategory");

        Map<String, String> catalogCategories = new TreeMap<String, String>();
        catalogCategories.put("all", "All");
        catalogCategories.put("acoustic", "Acoustic");
        catalogCategories.put("electric", "Electric");
        catalogCategories.put("classical", "Classical or Spanish");
        request.setAttribute("catalogCategories", catalogCategories);
        
        List<Product> productList;
        if (catalogCategory != null) {
            if (catalogCategory.equals("all")) {
                productList = ProductDB.getProducts();
            } else {
                productList = ProductDB.getProducts(catalogCategory);
            }
            request.setAttribute("productListOfCategory", productList);
            request.setAttribute("selectedCategory", catalogCategory);
        } else {


            request.setAttribute("selectedCategory", "all");
            productList = ProductDB.getProducts();
            request.setAttribute("productListOfCategory", productList);
        }
        if (productCode == null) {
            url = "/catalog.jsp";
        } else {
            //If there is a productCode parameter, validate that its value matches your product
            //code format and is a valid product code.

            long prodCode = Long.parseLong(productCode, 10);
            Product product = ProductDB.getProduct(prodCode);
            if (product != null) {
                url = "/item.jsp";
                request.setAttribute("product", product);
            } else {
                url = "/catalog.jsp";
            }

        }

        getServletContext()
                .getRequestDispatcher(url)
                .forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }
}
