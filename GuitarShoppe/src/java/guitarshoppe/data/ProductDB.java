/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guitarshoppe.data;

import guitarshoppe.business.Product;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author thomsonverghese
 */
public class ProductDB {

    public static Product getProduct(long productCode) {
        List<Product> productList = getProducts();

        for (Product product : productList) {
            if (product.getProductCode() == productCode) {
                return product;
            }
        }
        return null;
    }

    public static List<Product> getProducts() {
        LinkedList<Product> productList = new LinkedList<Product>();
        productList.add(new Product(1, "Ibanez GD10 Dreadnought Acoustic Guitar", "Acoustic", "The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.", 100, "/images/1.jpg"));
        productList.add(new Product(2, "Ibanez GD10 Dreadnought Acoustic Guitar", "Acoustic", "The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.", 150, "/images/2.jpg"));

        return productList;
    }

    public static List<Product> getProducts(String category) {
        LinkedList<Product> productList = new LinkedList<Product>();
        if (category.equals("acoustic")) {
            productList.add(new Product(1, "Ibanez GD10 Dreadnought Acoustic Guitar", "Acoustic", "The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.", 100, "/images/1.jpg"));
            productList.add(new Product(2, "Ibanez GD10 Dreadnought Acoustic Guitar", "Acoustic", "The Ibanez GD10 Dreadnought Acoustic Guitar is the perfect choice for players on a budget. It features a full-size dreadnought body that puts out plenty of balanced volume. The top is spruce with agathis back and sides for a tone that's deep and bright. The mahogany neck provides stability and sustain and has a durable rosewood fingerboard. Chrome tuners make tuning a breeze and the natural high gloss finish will protect your new instrument for years.", 150, "/images/2.jpg"));

        }
        return productList;
    }
}
